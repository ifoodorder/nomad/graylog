job "graylog" {
	datacenters = ["dc1"]
	type = "service"
	group "graylog" {
		network {
			port "http" {
				static = 9000
				to = 9000
			}
			port "beats" {
				static = 5044
				to = 5044
			}
		}
		service {
			name = "graylog"
			tags = ["logs"]
			port = "9000"
		}
		task "graylog" {
			vault {
				policies = ["graylog"]
			}
			user = "root"
			resources {
				memory = 2000
			}
			template {
				data =<<EOH
{{ with secret "pki_int/cert/ca_chain" }}{{ .Data.certificate }}
{{ end }}
				EOH
				destination = "local/ca_chain.pem"
			}
			template {
				data =<<EOH
{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=graylog" "common_name=graylog.service.consul" "ttl=24h" "alt_names=_graylog._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.certificate }}
{{ end }}{{ end }}
EOH
				destination = "local/cert.pem"
			}
			template {
				data =<<EOH
{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=graylog" "common_name=graylog.service.consul" "ttl=24h" "alt_names=_graylog._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.private_key }}
{{ end }}{{ end }}
EOH
				destination = "secrets/key.pem"
			}
			template {
				data = <<EOH
				# https://raw.githubusercontent.com/Graylog2/graylog-docker/4.1/config/graylog.conf
				is_master = true
				node_id_file = /usr/share/graylog/data/config/node-id
				password_secret = {{ with secret "secret/graylog/config" }}{{ .Data.secret }}
				root_password_sha2 = {{ .Data.sha2pass }}{{ end }}
				bin_dir = /usr/share/graylog/bin
				data_dir = /usr/share/graylog/data
				plugin_dir = /usr/share/graylog/plugin
				elasticsearch_hosts = https://{{ with secret "database/creds/graylog" }}{{ .Data.username }}:{{.Data.password}}{{ end }}@elasticsearch.service.consul:9200
				rotation_strategy = count
				elasticsearch_max_docs_per_index = 20000000
				elasticsearch_max_number_of_indices = 5
				retention_strategy = delete
				elasticsearch_shards = 1
				elasticsearch_replicas = 0
				elasticsearch_index_prefix = graylog
				allow_leading_wildcard_searches = false
				allow_highlighting = false
				elasticsearch_analyzer = standard
				output_batch_size = 500
				output_flush_interval = 1
				output_fault_count_threshold = 5
				output_fault_penalty_seconds = 30
				processbuffer_processors = 5
				outputbuffer_processors = 3
				processor_wait_strategy = blocking
				ring_size = 65536
				inputbuffer_ring_size = 65536
				inputbuffer_processors = 2
				inputbuffer_wait_strategy = blocking
				message_journal_enabled = true
				message_journal_dir = data/journal
				lb_recognition_period_seconds = 3
				mongodb_uri = mongodb://graylog:{{ with secret "secret/graylog/mongodb" }}{{ .Data.password }}{{ end }}@mongodb.service.consul:27017/graylog?ssl=true
				mongodb_max_connections = 1000
				mongodb_threads_allowed_to_block_multiplier = 5
				http_bind_address = 0.0.0.0:9000
				http_publish_uri = https://{{ env "NOMAD_ADDR_http" }}/
				http_external_uri = https://graylog.dev.ifoodorder.com/
				http_enable_tls = true
				http_tls_cert_file = /local/cert.pem
				http_tls_key_file = /secrets/key.pem

				EOH
				destination = "local/graylog.conf"
			}
			template {
				data = <<EOH
#!/bin/bash
keytool -importcert -keystore "$JAVA_HOME/lib/security/cacerts" -storepass changeit -file /local/ca_chain.pem -noprompt
su graylog
/docker-entrypoint.sh
EOH
				destination = "local/entrypoint.sh"
				perms = 555
			}
			driver = "docker"
			config {
				image = "graylog/graylog:4.1"
				volumes = ["local/graylog.conf:/usr/share/graylog/data/config/graylog.conf"]
				ports = ["http", "beats"]
				entrypoint = ["/local/entrypoint.sh"]
			}
		}
	}
}